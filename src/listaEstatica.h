#include <cstdlib>
#include <iostream>
#include <iomanip>

#define TIPO float
#define TMAX 40000

class ListaBasica { // Classe de Armazenamento
protected:

	TIPO v[TMAX]; // Vetor com os dados a serem armazenados
	unsigned short tam; // Tamanho ocupado da lista

public:

	ListaBasica() {
		tam = 0; // inicializa a lista com tamanho zero
	}

	int comprimento() { // necess�rio por tam ser privado
		return tam;
	}
};

class Lista: public ListaBasica { //Defini��o da Classe que herda da classe ListaBasica
public:
	TIPO primeiro() {
		return v[0];
	}

	TIPO ultimo() {
		return v[tam - 1]; //tam-1 por todo vetor come�ar em 0
	}

	bool localiza(TIPO val) {
		for (unsigned short i = 0; i < tam; i++)
			if (val == v[i])
				return true;
		return false;
	}

	int pos(TIPO val) {
		for (unsigned short i = 0; i < tam; i++)
			if (val == v[i])
				return i;
		return -1; //posi��o imposs�vel p/ sinalizar � encontrado
	}

	bool insere(TIPO val, unsigned short pos) {
		if ((pos >= 0) && (pos <= tam) && (tam < TMAX)) {
			for (unsigned short i = tam; i > pos; i--) // avan�a todos elementos
				v[i] = v[i - 1];
			v[pos] = val;  // grava o valor na posi��o
			tam++; // aumenta o tamanho do vetor
			return true;
		}
		return false;
	}

	bool insereI(TIPO val) {
		return insere(val, 0); //Chamada ao m�todo insere
	}

	bool insereF(TIPO val) {
		return insere(val, tam);
	}

	bool delP(unsigned short pos) {
		if ((pos >= 0) && (pos < tam)) {
			for (unsigned short i = pos; i < (tam - 1); i++)
				v[i] = v[i + 1];
			tam--; // diminui o tamanho do vetor
			return true;
		}
		return false;
	}
	bool delV(TIPO val) {
		return delP(pos(val));
	}

	TIPO ler(unsigned short pos) {
		if ((pos >= 0) && (pos < tam))
			return v[pos];
		return -1;
	}

	TIPO maximo() {
		TIPO max;
		if (tam == 0)
			return 0;
		else {
			max = v[0];
			for (int i = 0; i < tam; ++i) {
				if (v[i] > max)
					max = v[i];
			}
			return max;
		}
	}

	TIPO minimo() {
		TIPO min;
		if (tam == 0)
			return 0;
		else {
			min = v[0];
			for (int i = 0; i < tam; ++i) {
				if (v[i] < min)
					min = v[i];
			}
			return min;
		}
	}

	TIPO media() {
		TIPO med;
		if (tam == 0)
			return 0;
		else {
			for (int i = 0; i < tam; ++i) {
				med += v[i];
			}
			med = med/tam;
			return med;
		}
	}

	TIPO mediana() {
		TIPO mediana;

		if (tam == 0)
			return 0;

		else if ((tam) % 2 == 1) // N�mero impar de leituras: a mediana � o termo do meio
				{
			mediana = v[((tam + 1) / 2) - 1];
			return mediana;
		}

		else // N�mero par de leituras: a mediana � a media dos 2 termos do meio
		{
			mediana = v[(tam / 2) - 1] + v[(tam / 2)];
			mediana /= 2;
			return mediana;
		}
	}


}; // end Lista que herda da Lista B�sica
