#ifndef DATAAGORA_H
#define DATAAGORA_H


class DataAgora
{
    public:
            DataAgora();
            int getano();
		    int getmes();
		    int getdia();
		    int gethora();
		    int getmin();
		    int getseg();
    protected:

    private:
        int dia, mes, ano;
		int hora,minuto,segundo;

};

#endif // DATAAGORA_H
