
#ifndef MONITORATEMPERATURA_H_
#define MONITORATEMPERATURA_H_

#include <cstdlib>
#include <iostream>
#include "Temperatura.h"
#include "listaEstatica.h"


using namespace std;

class MonitoraTemperatura{
    private:

        int intervalo;
        int quantidade;
        double maxTemp;
        double minTemp;
        Lista listaTemp;
        Lista listaRes;
        Lista listaVent;
        int contmax;
        int contmin;

    public:
        MonitoraTemperatura();
        void setIntervalo(int intervalo);
        void setQuantidade(int quantidade);
        void realizarLeitura(Temperatura &simulador);
        void setTempLimitMax(double max);
        void setTempLimitMin(double min);
        void analisar();
        int getIntervalo();
        int getQuantidade();
        double getLimitMax();
        double getLimitMin();
        void lerLista(int modo);
        Lista getListaTemp();
        Lista getListaRes();
        void setListaTemp(Lista novaLista);
        void setListaRes(Lista novaLista);
        void setcontmin(int value);
        int getcontmin();
        void setcontmax(int value);
        int getcontmax();

};
#endif /* MONITORATEMPERATURA_H_ */
