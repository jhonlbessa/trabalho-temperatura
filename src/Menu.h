

#ifndef MENU_H_
#define MENU_H_

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

class Menu
{
public:

	Menu();

	int entradaValor(string message, int start, int end);
	void limpaTela();
	void validaEntrada(int first, int last, int &opt);
	void validaEntrada(double first, double last, double &val);

};
#endif
