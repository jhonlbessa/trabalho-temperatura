/*
 * Log.cpp
 *
 *  Created on: 15 de set de 2022
 *      Author: Wesley Krause
 */

#include "Log.h"
#include "DataAgora.h"
#include <string>

Log::Log() {

	filePath = ".\\logs\\";
	fstream logFile;

}

string Log::getFilePath(){
	return this->filePath;
}

void Log::setFilePath(string path){
	this->filePath = path;
}

void Log::write(string conteudo){
	logFile << conteudo << endl;
	cout << "Vai ser gravado isso: " << conteudo << endl;
}

void Log::openToWrite(){
	DataAgora data_atual;
	string filename = to_string(data_atual.getano())+to_string(data_atual.getmes())+to_string(data_atual.getdia())+"_"+to_string(data_atual.gethora())+to_string(data_atual.getmin())+to_string(data_atual.getseg())+".csv";
	string gravacao = this->filePath + filename;
	cout << "nome:" << gravacao << endl;
	logFile.open((this->filePath+filename),fstream::out);
}

void Log::openToRead(){
	logFile.open(this->filePath,fstream::in);
}

void Log::save(){
	logFile.close();
}

Lista Log::getlistatemparq(){

	string buf;
	string temp;
	unsigned short aux = 0;
	float Vtemp;
	Lista listatemp;

	do{
		logFile >> buf;
		temp = buf;

		temp.erase(7);
		temp.erase(0,1);
		temp = temp.replace(temp.find(','), 1, 1, '.');

		Vtemp = stof(temp);

		listatemp.insereF(Vtemp);
		cout << "LIDO DO ARQUIVO TXT :   Temperatura " << aux << " : "<< Vtemp << endl;
		cout << "LIDO DA LISTA :   TEMPERATURA " << aux << ": " << listatemp.ler(aux) << endl;
		cout << endl;
		aux++;

	}	while(!logFile.eof());
	return listatemp;
}

Lista Log::getlistaresarq(){

	bool Eres;
	bool Event;
	string buf;
	string res;
	unsigned short aux = 0;
	Lista listares;

	do{
		logFile >> buf;
		res = buf;

		res.erase(0,13);
		res.erase(7);

		if(res == "RES_ONN") {
			Eres = 1;
			Event = 0;
		}
		else {
			Eres = 0;
			Event = 1;
		}

		listares.insereF(Eres);
		cout << "LIDO DO ARQUIVO TXT-> ESTADO DO RES " << aux << ": "<< Eres << "|| estado da ventoinha: " << Event << endl;
		cout << "LIDO DA LISTA TXT-> ESTADO DO RES " << aux << ": " << listares.ler(aux) << endl;
		cout << endl;
		aux++;

	}	while(!logFile.eof());
	return listares;
}


