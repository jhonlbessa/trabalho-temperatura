#include "Gerenciador.h"
#include "Temperatura.h"
#include "MonitoraTemperatura.h"
#include "Menu.h"
#include "Log.h"


#include <windows.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <fstream>
#include <string>

using namespace std;

Gerenciador::Gerenciador() {

	Temperatura simulador;
	MonitoraTemperatura monitor;
	Menu menu;
	Log log;
	simulador.inicializa();

}

void Gerenciador::iniciar() {
cout<<"iniciar";
	int escolha;
	string message = "Escolha uma das opcoes:\n"
			"1 - Opcoes de leituras\n"
			"2 - Definir limites da temperatura\n"
			"3 - Guardar e Recuperar dados\n"
			"4 - Estudo de dados\n"
			"5 - Fechar programa\n";


	escolha = menu.entradaValor(message, 1, 5);

	if (escolha == 1)
		configLeituras();
	if (escolha == 2)
		controlar();
	if (escolha == 3)
		arquivar();
	if (escolha == 4)
		monitor.analisar();

	menu.limpaTela();
	if (escolha != 5)
		iniciar();

}
;

void Gerenciador::configLeituras() {
cout<<"config.leitura";
	int leituras;
	int tempo;
	int escolha;

	cout << endl;
	cout << "Menu 1 - Op��es de leituras" << endl;

	string message = "1 - Fazer leituras\n"
			"2 - Ler a lista\n"
			"3 - Retornar ao menu principal\n";

	escolha = menu.entradaValor(message, 1, 3);

	if (escolha == 1) {
		cout << "Insira  o n�mero de leituras: " << endl;

		menu.validaEntrada(1, 40000, leituras);
		monitor.setQuantidade(leituras);

		cout << "Insira o tempo (ms) entre cada leitura" << endl;

		menu.validaEntrada(1, 1000, tempo);
		monitor.setIntervalo(tempo);

		cout << "Configura��o realizada com sucesso!" << endl;
		Sleep(741);

		monitor.realizarLeitura(simulador);
	}

	if (escolha == 2) {
		int escala;

		cout << "1 - Ler as temperaturas em celsius" << endl;
		cout << "2 - Ler as temperaturas em fahrenheit" << endl;

		menu.validaEntrada(1, 2, escala);

		monitor.lerLista(escala);

	}

}
;

void Gerenciador::arquivar() {
cout<<"gerenc.arquivar";
	int escolha;
	int escolha2;
	string newpath;

	cout << endl;
	cout << "Menu 3 - Guardar e Recuperar dados" << endl;

	string message = "1 - Definir local de registro\n"
			"2 - Guardar dados em formato CSV\n"
			"3 - Ler dados salvo\n"
			"4 - Retornar ao menu principal\n";

	escolha = menu.entradaValor(message, 1, 4);

	if (escolha == 1) {
		cout<<"apenas define o nome do novo";
		cout << "Definir locais de registro\n" << endl;
		cout << "Local atual: " << log.getFilePath() << endl;

		string message = "1 - Escolher novo local de registro\n"
				"2 - Retornar ao menu de Configuracao\n";

		escolha2 = menu.entradaValor(message, 1, 2);

		if (escolha2 == 1) {
			cout << "Insira o novo local:" << endl;
			cin >> newpath;
			log.setFilePath(newpath);
		}
	}

	if (escolha == 2) {

		log.openToWrite();

		Lista TempLista = monitor.getListaTemp();
		Lista ResLista = monitor.getListaRes();

		int tamanho = TempLista.comprimento();
		double temp;
		bool res;
		string estadoRes = "";
		string line;

		for (int i = 0; i < tamanho; ++i) {

			temp = TempLista.ler(i);
			res = ResLista.ler(i);

			if (res == 1)
				estadoRes = "RES_ONN";
			if (res == 0)
				estadoRes = "RES_OFF";

			line = "\""+to_string(temp)+"\";\""+estadoRes+"\"";

			line = line.replace(line.find('.'), 1, 1, ',');

			log.write(line);

			Sleep(50);
		}
		log.save();
	}

	if (escolha == 3) {

		Lista Lteste;
		Lista Lteste2;

		log.openToRead();
		Lteste = log.getlistatemparq();
		log.save();
		log.openToRead();
		Lteste2 = log.getlistaresarq();
		log.save();

		monitor.setListaTemp(Lteste);
		monitor.setListaRes(Lteste2);

	}

}

void Gerenciador::controlar() {
cout<<"controlar";
	double tempmax;
	double tempmin;

	cout << endl;
	cout << "Menu 2 - Definir limites da temperatura\n" << endl;
	cout << "Temperatura m�xima atual: " << monitor.getLimitMax() << "�C"
			<< endl;
	cout << "Temperatura m�nima atual: " << monitor.getLimitMin() << "�C"
			<< endl;

	unsigned int escolha;
	string message = "1 - Definir novos limites\n"
			"2 - Retornar ao menu principal\n";

	escolha = menu.entradaValor(message, 1, 2);

	if (escolha == 1) {

		cout << "Insira o valor de temperatura m�xima: " << endl;
		menu.validaEntrada(1, 300, tempmax);
		monitor.setTempLimitMax(tempmax);

		cout << "Insira o valor de temperatura m�nima: " << endl;
		menu.validaEntrada(1, (monitor.getLimitMax() - 1), tempmin);
		monitor.setTempLimitMin(tempmin);

		cout << "Temperaturas  com sucesso!" << endl;
		Sleep(741);

	}
}

