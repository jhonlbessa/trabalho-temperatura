

#include "MonitoraTemperatura.h"

MonitoraTemperatura::MonitoraTemperatura() {
	// TODO Auto-generated constructor stub

	intervalo = 15;
	quantidade = 100;
	maxTemp = 99;
	minTemp = 1;
	//statusControle = 1;
	Lista listaTemp;
	Lista listaRes;
	contmax = 0;
	contmin = 0;
}

void MonitoraTemperatura::setIntervalo(int intervalo){
cout<<"intervalo";	this->intervalo = intervalo;
}

int MonitoraTemperatura::getIntervalo(){
	return this->intervalo;
}

void MonitoraTemperatura::setQuantidade(int quantidade){
cout<<"quantidade";	this->quantidade = quantidade;
}

int MonitoraTemperatura::getQuantidade(){
	return this->quantidade;
}

void MonitoraTemperatura::setTempLimitMax(double max){
cout<<"monitoratemp.settempmax: seta o limite maximo";	this->maxTemp = max;
}

void MonitoraTemperatura::setTempLimitMin(double min){
	cout<<"monitoratemp.settempmin: seta o limite min";	this->minTemp = min;
}

double MonitoraTemperatura::getLimitMax(){
	return this->maxTemp;
}

double MonitoraTemperatura::getLimitMin(){
	return this->minTemp;
}

void MonitoraTemperatura::realizarLeitura(Temperatura &simulador) {

	simulador.enviarComando(RES_ON);
	bool res = 1;

	for (int i = 0; i < quantidade; ++i) {

		float temp = simulador.lerTemp();

		if (temp > this->maxTemp) {
			simulador.enviarComando(VENT_ON);
			simulador.enviarComando(RES_OFF);
			res = 0;
			this -> contmax++;
		}
		if (temp < this->minTemp) {
			simulador.enviarComando(VENT_OFF);
			simulador.enviarComando(RES_ON);
			res = 1;
			this -> contmin++;
		}

		listaTemp.insereF(temp);
		listaRes.insereF(res);

		cout << "Temperatura: " << temp << "; " << "Estado do Resistor: " << res << "; " << "Estado da ventoinha: " << !res << ";" << endl;

		Sleep(this->intervalo);
	}

	Sleep(741);
	simulador.enviarComando(RES_OFF);
	simulador.enviarComando(VENT_OFF);

}

void MonitoraTemperatura::analisar(){

	cout << endl;
	double tempmax     = listaTemp.maximo();
	double tempmin     = listaTemp.minimo();
	double tempmedia   = listaTemp.media();
	double tempmediana = listaTemp.mediana();


	cout << "4 - Analise dos dados (Temperatura maxima, minima, media e mediana)" << endl;

	cout << "Temperatura maxima atingida: " << tempmax << "�C" << endl;
	cout << "Temperatura minima atingida: " << tempmin << "�C" << endl;
	cout << "Temperatura media:           " << tempmedia << "�C" << endl;
	cout << "temperatura mediana:         " << tempmediana << "�C" << endl;
	cout << "Numero de ultrapassagens da temperatura maxima:" << this->contmax << endl;
	cout << "Numero de ultrapassagens da temperatura minima:" << this->contmin << endl;

	cout << endl;
	Sleep(2000);

}

void MonitoraTemperatura::lerLista(int modo) {


	cout << "Dados Guardados na lista:" << endl;
	Sleep(741);
	float temp;
	string carac = "°C";
	if (modo == 2) carac = "°F";
	int tam = listaTemp.comprimento();
	if(tam == 0) cout << "Lista Vazia!!";

 	for (int i = 0; i < tam; i++) {
		Sleep(20);
		temp = listaTemp.ler(i);
		if(modo == 2){
			temp = ((temp*1.8) + 32);
		}
		cout << "Temperatura: " << temp << carac << "  Resistor: " << listaRes.ler(i) << endl;
 	}

	Sleep(1000);
}

Lista MonitoraTemperatura::getListaTemp() {
	return listaTemp;
}

Lista MonitoraTemperatura::getListaRes() {
	return listaRes;
}

void MonitoraTemperatura::setListaTemp(Lista novaLista){
	this->listaTemp = novaLista;
}

void MonitoraTemperatura::setListaRes(Lista novaLista){
	this->listaRes = novaLista;
}
void MonitoraTemperatura::setcontmin(int value){
	this->contmin = value;
}
int MonitoraTemperatura::getcontmin(){
	return this->contmin;
}
void MonitoraTemperatura::setcontmax(int value){
	this->contmax = value;
}

int MonitoraTemperatura::getcontmax(){
	return this->contmax;
}
