#ifndef GERENCIADOR_H_
#define GERENCIADOR_H_

#include "Gerenciador.h"
#include "Temperatura.h"
#include "MonitoraTemperatura.h"
#include "Menu.h"
#include "Log.h"

#include <windows.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <fstream>
#include <string>

class Gerenciador {

	private:
	Temperatura simulador;
	MonitoraTemperatura monitor;
	Menu menu;
	Log log;

public:

	Gerenciador();
	void iniciar();
	void configLeituras();
	void arquivar();
	void controlar();

};


#endif /* GERENCIADOR_H_ */
