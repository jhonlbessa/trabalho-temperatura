

#include "Menu.h"

Menu::Menu() {
	// TODO Auto-generated constructor stub

}

int Menu::entradaValor(string message, int start, int end) {

	int opt;

	cout << "\n" << message;
	cin >> opt;
	while (cin.fail() || opt < start || opt > end) {
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "Op��o inv�lida! Tente novamente " << start << "-" << end << endl;
		cin >> opt;
	}
	return opt;
}

void Menu::validaEntrada(int first, int last, int &opt){

	cin >> opt;
	while (cin.fail() || opt < first || opt > last)
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "Op��o inv�lida! Tente novamente " << first << "-" << last << endl;
		cin >> opt;
	}

}

void Menu::validaEntrada(double first, double last, double &val){

	cin >> val;
	while (cin.fail() || val < first || val > last)
	{
		cin.clear();
		cin.ignore(1000, '\n');
		cout << "Op��o inv�lida! Tente novamente " << first << "-" << last << endl;
		cin >> val;
	}

}

void Menu::limpaTela(){
	//system("cls");		// n funcionou
	//system("clear");    	//tmb n
	cout << " \n\n\n\n\n\n " << endl; // "limpar" assim mesmo
}

