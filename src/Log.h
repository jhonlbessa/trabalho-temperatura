
#ifndef LOG_H_
#define LOG_H_
#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>
#include "MonitoraTemperatura.h"

using namespace std;

class Log {
private:

	string basePath;
	string filePath;

public:

	fstream logFile;


	Log();

	void setFilePath(string path);
	string getFilePath();
	void write(string conteudo); //salva o arquivo com o nome e no diretorio definido em pathFile
	void openToWrite();
	void openToRead();
	void save();

	Lista getlistatemparq();
	Lista getlistaresarq();

};

#endif /* LOG_H_ */
